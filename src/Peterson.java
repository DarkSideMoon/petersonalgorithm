import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Created by ����� on 01.12.2015.
 */
public class Peterson implements Lock {

    public boolean[] flag = new boolean[2];
    public int turn;

    public Peterson(){
        flag[0] = false;
        flag[1] = false;

        turn = 1;
    }

    @Override
    public void lock() {
        flag[0] = false;
        flag[1] = false;

        turn = 1;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public void unlock() {

    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
