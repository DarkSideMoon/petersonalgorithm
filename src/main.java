/**
 * Created by ����� on 01.12.2015.
 */
public class Main {
    public static void main(String[] args) {
        Peterson p = new Peterson();

        WorkerProcess p1 = new WorkerProcess(0, p);
        WorkerProcess p2 = new WorkerProcess(1, p);

        Thread t1 = new Thread(p1);
        Thread t2 = new Thread(p2);

        // start the threads
        // example 1 => work in successively (���������������)
        /*
        t1.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t2.start();

        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        */

        t1.start();
        t2.start();
    }

}
