/**
 * Created by ����� on 01.12.2015.
 */
public class WorkerProcess implements Runnable {

    private int count = 0;

    private int id = 0;
    private volatile Peterson sharedInstance; // shared variables instance

    public WorkerProcess(int id, Peterson instance) {
        this.id = id;
        this.sharedInstance = instance;
    }

    @Override
    public void run() {
        while (true) {
            this.count++;

            // express desire to enter cs
            sharedInstance.flag[id] = true;

            // get the id of the other process
            int otherProcessID = (id == 0 ? 1 : 0);

            // tell the other process it your turn to enter cs
            sharedInstance.turn = id;

            // this while condition is very important and this is where the crux
            // of the algorithm lies
            // Case 1: Lets say P0 wants to enter but P1 doesnt: In this case,
            // sharedInstance.flag[1] == false
            // and P0 will happily enter cs

            // Case 2: If P1 wants to enter but P0 doesnt. In this case,
            // sharedInstance.flag[0] == false
            // and P1 happily

            // Case 3: If P0 makes flag[0] = true and turn = 0 and at the same
            // time P1 makes flag[1] = true and turn = 1
            // then P0 should get priority and P1 must wait. This is taken care
            // by the while condition

            // Case 4: if P1 makes flag[1] = true and turn = 1 and at the same
            // time P0 makes flag[0] = true and turn = 0
            // then P1 must get priority and P0 must wait.

            while (sharedInstance.flag[otherProcessID] == true
                    && sharedInstance.turn == id)
                ; // busy wait print to console => show a lot of output processes

            // enter cs
            this.process(count);

            // announce that you're done executing the critical section
            sharedInstance.flag[id] = false;

            if(this.count == 10) {
                System.out.println("----------------STOP! PROCESSES----------------");
                break;
            }

        }
    }

    private void process(int count) {
        try {
            System.out.println("Process #: " + this.id + " entering to critical section. Sleep 1 sec. [" + count + "]");
            Thread.currentThread();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
